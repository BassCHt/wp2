<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_wp2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/?M6ygrJGF3ttf%=~IXR7&*4|qH,_VDbUek#J`@-pWKSdiW#|1/wsMcs8<v:)f+Z');
define('SECURE_AUTH_KEY',  '/8R97O+Ed!ptqZ?ZM)I_1C@:zfSHrCzzry&F17fh5-A[dz*|GAH`DZf`Lw{,!,j,');
define('LOGGED_IN_KEY',    '~)Wa80o(|5NsR#yC2JYLy0`|Ptqo rdk%78JZFte.uK5iLn,`Z30FUwB=ID2h:x{');
define('NONCE_KEY',        '^4,7yj8^{|=^]KnI)EHx4#D#1RY<cNnuxBFV<9xw1Ju?+bs`Snw)^K <!q2I+K|^');
define('AUTH_SALT',        'EQaj%.OSkZ%DmxlI~d*yakp!lC%/C3UW05$<>g821NnJ0gv&sPE:GFX7$22QWbL ');
define('SECURE_AUTH_SALT', ';*}ej2N/&9+<EcaoM7WP&|?TVCe1WfS% 6uJT0KtKja<*CDEkC~q=BA3w1AB!Qe)');
define('LOGGED_IN_SALT',   '+vX0oOvB(Tgt!x?SeIi%EpOE@oV-qbu]^1Y8:,( :Cx8gM+r?iX-W[t%l#4@,E=A');
define('NONCE_SALT',       '2FS:559/?-n;C!,2@w7^{`p9YAXYQK9i7%[z6c}1d:2fH[c>h $Rmb<MX/?_h7R6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'twp2_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
