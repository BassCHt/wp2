<?php
if ( !defined( 'ABSPATH' ) ) exit;
/**
* Icegram Campaign Admin class
*/
if ( !class_exists( 'Icegram_Compat_icegram_rainmaker' ) ) {
	class Icegram_Compat_icegram_rainmaker extends Icegram_Compat_Base {

		function __construct() {
			global $icegram; 
			parent::__construct();
		}

		function render_js() {
			?>
			<script type="text/javascript">
			jQuery(function() {
			  	jQuery( window ).on( "init.icegram", function(e, ig) {
				  	// Find and init all RM forms within Icegram messages/divs
				  	if(ig){
					  	jQuery.each(ig.messages, function(i, msg){
					  		var forms = jQuery(msg.el).find('.rainmaker_form form');
					  		forms.each(function(i, form){
						  		if(!jQuery(form).hasClass('rm_init_done')){
									window.rainmaker.init(form);
						  			jQuery(form).addClass('rm_init_done');
						  		}
					  		});
					  	});
				  	}

			  	}); // init.icegram
				
				//Handle CTA function(s) after successful submission of form
			  	jQuery( window ).on('success.rm', function(e, form, response) {
			  		if( typeof icegram !== 'undefined'){
			  			var msg_id = (jQuery(e.target.closest('[id^=icegram_message_]')).attr('id') || '').split('_').pop() || 0 ;
		  				var msg = icegram.get_message_by_id(msg_id) || undefined;
					  	if(typeof msg !== 'undefined'){
						  	if(msg.data.cta === 'form_via_ajax' && msg.el.find('.rm_subscription').length > 0){
					  			msg.el.trigger('form_success.ig_cta', [msg]);
						  	} else if(msg.data.cta === 'form' || !msg.data.cta){
						  		response_text = '<div class="ig_form_response_text">'+ (msg.data.response_text || msg.el.find('.rm_form_message').html() || '') +'</div>';
								msg.el.find('.ig_form_container, .ig_message, .ig_headline').fadeOut();
								var	appendTo = msg.el.filter('.ig_container');
								if(jQuery.inArray(msg.data.type, ['interstitial', 'messenger']) !== -1){
									appendTo = msg.el.find('.ig_message');
									appendTo.show();
									msg.el.find('.ig_headline').text('').show();
								}else if(msg.data.type === 'tab'){
									//TODO :: hide is not working 
									appendTo = msg.el.find('.ig_data');
									msg.el.find('.ig_headline').show();
								}
								try{
									appendTo.append(response_text);
								}catch(err){
									console.log(err);
								}
						  	}
				  		}

			  		}
				}); //success.rm

			});

			</script>

		<?php
		}
	}
}